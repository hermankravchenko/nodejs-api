const { fighter } = require('../models/fighter');
const { body, param, validationResult } = require('express-validator');

const createFighterValid = () => {
    return [
        body('name')
            .not()
            .isEmpty()
            .withMessage('Name field must not be empty')
            .trim(),
        body('health')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Health field must not be empty')
            .isInt({min: 1, max: 99})
            .withMessage('Health must be between 1 and 100'),
        body('power')
            .not()
            .isEmpty()
            .withMessage('Power field must not be empty')
            .isInt({min: 1, max: 99})
            .withMessage('Power must be between 1 and 99'),
        body('defense')
            .not()
            .isEmpty()
            .withMessage('Defense field must not be empty')
            .isInt({min: 1, max: 10})
            .withMessage('Defense must be between 1 and 10'),
        body('Id')
            .not()
            .exists()
            .withMessage("Id field must not be provided"),
        body('id')
            .not()
            .exists()
            .withMessage("Id field must not be provided"),

    ];
}

const findFighterById = () => {
    return [
        param('id')
            .not()
            .isEmpty()
            .withMessage('Id parameter must be provided')
            .trim()
            .toLowerCase()
            .isLength({min: 30, max: 36})
            .withMessage('id must be between 30 and 36 characters long')
    ];
}

const updateFighterValid = () => {
    return [
        body('name')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Name field must not be empty')
            .trim(),
        body('health')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Health field must not be empty')
            .isInt({min: 1, max: 99})
            .withMessage('Health must be between 1 and 100'),
        body('power')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Power field must not be empty')
            .isInt({min: 1, max: 99})
            .withMessage('Power must be between 1 and 99'),
        body('defense')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Email field must not be empty')
            .isInt({min: 1, max: 10})
            .withMessage('Defense must be between 1 and 10'),
        body('Id')
            .not()
            .exists()
            .withMessage("Id field must not be provided"),
        body('id')
            .not()
            .exists()
            .withMessage("Id field must not be provided")
    ];
}

const validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push(err.msg))
    return res.status(400).json({
        error: true,
        message: JSON.stringify(extractedErrors),
    })
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.findFighterById = findFighterById;
exports.validate = validate;