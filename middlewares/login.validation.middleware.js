const { body, validationResult } = require('express-validator');

const userValidationRules = () => {
    return [
        body('email')
            .trim()
            .not()
            .isEmpty()
            .withMessage('Email field must not be empty')
            .isEmail()
            .withMessage('Email must be in the correct format')
            .toLowerCase(),
        body('password')
            .not()
            .isEmpty()
            .withMessage('Password field must not be empty')
            .isLength({ min: 3 })
            .withMessage('Password must be at least 3 characters long')
            .matches(/^[A-Za-z0-9]{3,30}$/)
            .withMessage('Password must be 3 to 30 characters long and have only lowercase, uppercase, and numbers'),
    ]
}

const validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

    return res.status(400).json({
        errors: extractedErrors,
    })
}

module.exports = {
    userValidationRules,
    validate,
}