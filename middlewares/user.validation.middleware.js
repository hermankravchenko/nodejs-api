const { user } = require('../models/user');
const { body, param, validationResult } = require('express-validator');

const createUserValid = () => {
    return [
        body('firstName')
            .not()
            .isEmpty()
            .withMessage('First Name field must not be empty')
            .trim(),
        body('lastName')
            .not()
            .isEmpty()
            .withMessage('Last Name field must not be empty')
            .trim(),
        body('phoneNumber')
            .not()
            .isEmpty()
            .withMessage('Phone number field must not be empty')
            .matches(/\+380*[0-9]{9}$/)
            .withMessage("Phone number must be formatted correctly +380xxxxxxxxx"),
        body('email')
            .not()
            .isEmpty()
            .withMessage('Email field must not be empty')
            .trim()
            .isEmail()
            .withMessage('Email must be in the correct format')
            .toLowerCase()
            .matches(/^[\w.+\-]+@gmail\.com$/)
            .withMessage("Email must be gmail.com domain only")
            .normalizeEmail({gmail_lowercase: true, gmail_convert_googlemaildotcom: true}),
        body('password')
            .not()
            .isEmpty()
            .withMessage('Password field must not be empty')
            .isLength({ min: 3 })
            .withMessage('Password must be at least 3 characters long')
            .matches(/^[A-Za-z0-9]{3,30}$/)
            .withMessage('Password must be 3 to 30 characters long and have only lowercase, uppercase, and numbers'),
        body('id')
            .not()
            .exists()
            .withMessage("Id field must not be provided"),
    ];
}

const findUserById = () => {
    return [
        param('id')
            .not()
            .isEmpty()
            .withMessage('Id parameter must be provided')
            .trim()
            .toLowerCase()
            .isLength({min: 30, max: 36})
            .withMessage('id must be between 30 and 36 characters long')
       ];
}

const updateUserValid = () => {
    return [
        body('firstName')
            .optional()
            .not()
            .isEmpty()
            .withMessage('First Name field must not be empty')
            .trim(),
        body('lastName')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Last Name field must not be empty')
            .trim(),
        body('phoneNumber')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Phone number field must not be empty')
            .matches(/\+380*[0-9]{9}$/)
            .withMessage("Phone number must be formatted correctly +380xxxxxxxxx"),
        body('email')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Email field must not be empty')
            .trim()
            .isEmail()
            .withMessage('Email must be in the correct format')
            .toLowerCase()
            .matches(/^[\w.+\-]+@gmail\.com$/)
            .withMessage("Email must be gmail.com domain only")
            .normalizeEmail({gmail_lowercase: true, gmail_convert_googlemaildotcom: true}),
        body('password')
            .optional()
            .not()
            .isEmpty()
            .withMessage('Password field must not be empty')
            .isLength({ min: 3 })
            .withMessage('Password must be at least 3 characters long')
            .matches(/^[A-Za-z0-9]{3,30}$/)
            .withMessage('Password must be 3 to 30 characters long and have only lowercase, uppercase, and numbers'),
        body('id')
            .not()
            .exists()
            .withMessage("Id field must not be provided")
    ];
}

const validate = (req, res, next) => {

    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push(err.msg))
    return res.status(400).json({
        error: true,
        message: JSON.stringify(extractedErrors),
    })
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.findUserById = findUserById;
exports.validate = validate;