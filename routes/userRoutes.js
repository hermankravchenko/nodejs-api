const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, validate, updateUserValid, findUserById } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid(), validate, (req, res, next) => {
    try {
        if (UserService.search(req.body)){
            return res.status(400).json({
                error: true,
                message: 'User with the email address exists in the system'
            });
        }
        if (req.body.phoneNumber) {
            const phone = UserService.phoneVerification(req.body.phoneNumber);
            if (phone) {
                res.status(400).json({
                    error: true,
                    message: "Phone number must be unique"
                });
            }
        }
        const data = UserService.create(req.body);
        console.log(data);
        res.status(200).json(data);
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

router.get('/:id', findUserById(), validate, (req, res, next) => {
    try {
        const data = UserService.searchById(req.params);
        if (data) {
            res.status(200).json(data);
        } else {
            res.status(404).json({
                error: true,
                message: 'User not found'
            });
        }

    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
       try {
           const data = UserService.getUsers();
           if (data.length > 0) {
               res.status(200).json(data);
           } else {
               res.status(404).json({
                   error: true,
                   message: 'No users found in the database'
               });
           }

       } catch (err) {
           res.status(400).err.json(err);
       } finally {
           next();
       }
});

router.put('/:id', findUserById(), updateUserValid(), validate, (req, res, next) => {
    try {
        if (UserService.searchById(req.params)){
            if (req.body.phoneNumber) {
                const phone = UserService.phoneVerification(req.body.phoneNumber);
                if (phone) {
                    res.status(400).json({
                        error: true,
                        message: "Phone number must be unique"
                    });
                }
            }
            const data = UserService.update(req.params.id, req.body);
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({
                    error: true,
                    message: "User update failed"
                });
            }
        } else {
            res.status(404).json({
                error: true,
                message: "User not found"
            });
        }
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

router.delete('/:id', findUserById(), validate, (req, res, next) => {
    try {
        if (UserService.searchById(req.params)){
            const data = UserService.delete(req.params);
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({
                    error: true,
                    message: "User delete failed"
                });
            }
        } else {
            res.status(404).json({
                error: true,
                message: "User not found"
            });
        }
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

module.exports = router;