const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, validate, updateFighterValid, findFighterById } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', createFighterValid(), validate, (req, res, next) => {
    try {
        const data = FighterService.create(req.body);
        console.log(data);
        res.status(200).json(data);
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

router.get('/:id', findFighterById(), validate, (req, res, next) => {
    try {
        const data = FighterService.searchById(req.params);
        console.log(data);
        if (data) {
            res.status(200).json(data);
        } else {
            res.status(404).json({
                error: true,
                message: 'Fighter not found'
            });
        }

    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        const data = FighterService.getUsers();
        if (data.length > 0) {
            res.status(200).json(data);
        } else {
            res.status(404).json({
                error: true,
                message: 'No fighters found in the database'
            });
        }

    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

router.put('/:id', findFighterById(), updateFighterValid(), validate, (req, res, next) => {
    try {
        if (FighterService.searchById(req.params)){
            console.log('here');
            const data = FighterService.update(req.params.id, req.body);
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({
                    error: true,
                    message: "Fighter update failed"
                });
            }
        } else {
            res.status(404).json({
                error: true,
                message: "Fighter not found"
            });
        }
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

router.delete('/:id', findFighterById(), validate, (req, res, next) => {
    try {
        if (FighterService.searchById(req.params)){
            const data = FighterService.delete(req.params);
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({
                    error: true,
                    message: "Fighter delete failed"
                });
            }
        } else {
            res.status(404).json({
                error: true,
                message: "Fighter not found"
            });
        }
    } catch (err) {
        res.status(400).err.json(err);
    } finally {
        next();
    }
});

module.exports = router;