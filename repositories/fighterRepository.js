const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    getOneById(data) {
        return this.dbContext.find(({ id }) => id === data.id).value();
    }
}

exports.FighterRepository = new FighterRepository();