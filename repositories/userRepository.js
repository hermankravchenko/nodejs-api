const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
    // override getOne to search for an email and password
    getOne(user) {
        return this.dbContext.find(({ email }) => email === user.email).value();
        // this.dbContext.find(({ password }) => password === user.password).value();
        // TODO: password verification (hash verification if there is enough time
    }

    getOneById(user) {
        return this.dbContext.find(({ id }) => id === user.id).value();
    }

    verifyPhoneUnique(data) {
        return this.dbContext.find(({ phoneNumber }) => phoneNumber === data.phoneNumber).value();
    }
}

exports.UserRepository = new UserRepository();