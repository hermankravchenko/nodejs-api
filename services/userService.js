const { UserRepository } = require('../repositories/userRepository');

class UserService {

    search(userData) {
        if (userData && userData.email && userData.password) {
            const tempObj = {
                email: userData.email,
                password: userData.password
            };
            const item = UserRepository.getOne(tempObj);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }

    searchById(userData) {
        if (userData && userData.id) {
            const tempObj = {
                id: userData.id
            };
            const item = UserRepository.getOneById(tempObj);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }

    create(userData) {
        if (userData && userData.email && userData.password &&
            userData.firstName && userData.lastName && userData.phoneNumber) {
            const tempUser = {
                firstName: userData.firstName,
                lastName: userData.lastName,
                phoneNumber: userData.phoneNumber,
                email: userData.email,
                password: userData.password
            }
            const user = UserRepository.create(tempUser);
            return user;
        } else {
            return null;
        }
    }

    getUsers() {
        if (UserRepository.getAll()) {
            return UserRepository.getAll();
        } else
            return null;
    }

    update(userID, userData) {
        if (userID && userData && (userData.email || userData.password ||
            userData.firstName || userData.lastName || userData.phoneNumber)) {
            const allowed = [ 'firstName', 'lastName','email','phoneNumber','password'];
            // filter to only allowed fields
            const tempData = allowed
                .reduce((obj, key) => ({ ...obj, [key]: userData[key] }), {});
            // remove any undefined or empty fields to not rewrite correct data in the db
            const tempDataFilt = obj =>
                Object.entries(obj)
                    .filter(([_, value]) => !!value)
                    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
            const user = UserRepository.update(userID, tempDataFilt(tempData));
            return user;
        } else {
            return null;
        }
    }

    delete(userData) {
        if (userData && userData.id) {
            const item = UserRepository.delete(userData.id);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }

    phoneVerification(userData) {
        console.log(userData);
        if (userData) {
            const tempObj = {
                phoneNumber: userData
            };
            const item = UserRepository.verifyPhoneUnique(tempObj);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }
}



module.exports = new UserService();