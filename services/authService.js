const UserService = require('./userService');

class AuthService {
    login(userData) {
        if (userData && userData.email && userData.password) {
            const tempObj = {
                email: userData.email,
                password: userData.password
            };
            const user = UserService.search(tempObj);
            if(!user) {
                throw Error('User not found');
            }
            return user;
        }

    }
}

module.exports = new AuthService();