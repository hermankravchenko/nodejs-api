const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    searchById(data) {
        if (data && data.id) {
            const tempObj = {
                id: data.id
            };
            const item = FighterRepository.getOneById(tempObj);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }

    create(data) {
        if (data && data.name &&
            data.power && data.defense || data.health) {
            const tempObj = {
                name: data.name,
                health: data.health,
                power: data.power,
                defense: data.defense
            }
            const tempDataFilt = obj =>
                Object.entries(obj)
                    .filter(([_, value]) => !!value)
                    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
            const user = FighterRepository.create(tempDataFilt(tempObj));
            return user;
        } else {
            return null;
        }
    }

    getUsers() {
        if (FighterRepository.getAll()) {
            return FighterRepository.getAll();
        } else
            return null;
    }

    update(id, data) {
        if (id && data && (data.name || data.health ||
            data.power || data.defense)) {
            const allowed = [ 'name', 'health','power','defense'];
            // filter to only allowed fields
            const tempData = allowed
                .reduce((obj, key) => ({ ...obj, [key]: data[key] }), {});
            // remove any undefined or empty fields to not rewrite correct data in the db
            const tempDataFilt = obj =>
                Object.entries(obj)
                    .filter(([_, value]) => !!value)
                    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
            const item = FighterRepository.update(id, tempDataFilt(tempData));
            return item;
        } else {
            return null;
        }
    }

    delete(data) {
        if (data && data.id) {
            const item = FighterRepository.delete(data.id);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null;
        }
    }
}

module.exports = new FighterService();